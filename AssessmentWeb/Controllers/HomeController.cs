﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AssessmentWeb.Repositories;
using AssessmentWeb.Models;
using AssessmentWeb.Helpers;
namespace AssessmentWeb.Controllers
{
    public class HomeController : Controller
    {
        private IHomeRepository home;

        #region Constructor
        public HomeController(IHomeRepository home)
        {
            this.home = home;
        }
        #endregion

        #region public methods
        // GET: Home
        public ActionResult Index()
        {
            return View(Constants.ViewName.index);
        }

        public PartialViewResult Update(int val)
        {
            //Submit the data to API
            this.home.Submit(val);
            //calculate the input val
            IList<Quiz> quizList = this.home.Calculate(val);
            //Return partial view
            //return PartialView("~/Views/Home/_Status.cshtml", quizList);
            return PartialView(Constants.ViewName.status, quizList);
        }
        #endregion
    }
}
﻿var ApiHelper = new function () {
    this.callPostAPI = function (url, content) {
        var dr = { 'val': content };
        $.ajax({
            url: url,
            type: 'POST',
            dataType: "html",
            data: dr,
            async: true,
            cache: false,
            success: function (result) {
                $("#divResult").html(result);
                console.log("Success in ApiHelper calling: " + url + "; result: " + result);
            },
            error: function (result) {
                console.log("Error in ApiHelper calling: " + url + "; result: " + result);
            }
        });
    };

    this.ProcessComplexJsonObject =  function (obj) {
        var result = {};
        var buildResult = function (object, prefix) {
            for (var key in object) {
                var postKey = isFinite(key) ? (prefix != "" ? prefix : "") + "[" + key + "]" : (prefix != "" ? prefix + "." : "") + key;
                switch (typeof (object[key])) {
                    case "number": case "string": case "boolean":
                        result[postKey] = object[key];
                        break;
                    case "object":
                        if (object[key] != null && object[key] != undefined && [key].toUTCString)
                            result[postKey] = object[key].toUTCString().replace("UTC", "GMT");
                        else {
                            buildResult(object[key], postKey != "" ? postKey : key);
                        }
                }
            }
        };
        buildResult(obj, "");
        return result;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AssessmentWeb.Models;
namespace AssessmentWeb.Repositories
{
    public interface IHomeRepository
    {
        IList<Quiz> Calculate(int val);
        void Submit(int val);
    }
}
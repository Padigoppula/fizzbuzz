﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AssessmentWeb.Repositories;
using Moq;
using System.Collections.Generic;
using AssessmentWeb.Models;

namespace AssessmentWebTest.Repository
{
    [TestClass]
    public class HomeRepositorytest
    {
        public IHomeRepository _homeRepositorymock;

        [TestInitialize]
        public void Initialize()
        {
            //Instantiate object
            _homeRepositorymock = new HomeRepository();
        }

        [TestMethod]
        public void Calculate_test()
        {
            int val = 5;
            
            //Calling the method
            var lst = _homeRepositorymock.Calculate(val);

            Assert.IsNotNull(lst);
            Assert.IsTrue(lst.Count > 0);
        }
    }
}

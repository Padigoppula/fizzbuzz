﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using AssessmentWeb.Repositories;
using AssessmentWeb.Controllers;
using System.Web.Mvc;
using AssessmentWeb.Models;
using System.Linq;
namespace AssessmentWebTest.Controllers
{
    /// <summary>
    /// Summary description for HomeControllerTest
    /// </summary>
    [TestClass]
    public class HomeControllerTest
    {
        private Mock<IHomeRepository> _homeRepositorymock;

        [TestInitialize]
        public void Initialize()
        {
            _homeRepositorymock = new Mock<IHomeRepository>();
        }

        [TestMethod()]
        public void Index_Test()
        { 
          
            var homeController = new HomeController(_homeRepositorymock.Object);
            ViewResult result = homeController.Index() as ViewResult;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewName.Contains("Index"));
        }

        [TestMethod]
        public void Update_Returns_ViewName_test()
        {
            List<Quiz> lst= new List<Quiz>();
            lst.Add(new Quiz { QuizName = "Fizz" });
            lst.Add(new Quiz { QuizName = "Buzz" });
            _homeRepositorymock.Setup(s => s.Calculate(It.IsAny<int>())).Returns(lst);
            _homeRepositorymock.Setup(x => x.Submit(It.IsAny<int>()));
            var homeController = new HomeController(_homeRepositorymock.Object);
            var result = homeController.Update(It.IsAny<int>()) as PartialViewResult;

            Assert.IsNotNull(result);
            Assert.IsTrue(result.ViewName.ToLowerInvariant().Contains("_status"));
        }

        [TestMethod]
        public void Update_Returns_Model_test()
        {
            List<Quiz> lst = new List<Quiz>();
            lst.Add(new Quiz { QuizName = "Fizz" });
            lst.Add(new Quiz { QuizName = "Buzz" });
            _homeRepositorymock.Setup(s => s.Calculate(It.IsAny<int>())).Returns(lst);
            _homeRepositorymock.Setup(x => x.Submit(It.IsAny<int>()));
            var homeController = new HomeController(_homeRepositorymock.Object);
            var result = homeController.Update(It.IsAny<int>()) as PartialViewResult;

            Assert.IsNotNull(result.Model);
            Assert.AreEqual(((List<Quiz>)result.Model).Count(), 2 );
            
        }
    }
}
